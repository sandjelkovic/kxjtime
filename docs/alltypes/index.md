

### All Types

| Name | Summary |
|---|---|
| [java.time.Duration](../com.sandjelkovic.kxjtime/java.time.-duration/index.md) (extensions in package com.sandjelkovic.kxjtime) |  |
| [kotlin.Int](../com.sandjelkovic.kxjtime/kotlin.-int/index.md) (extensions in package com.sandjelkovic.kxjtime) |  |
| [kotlin.Long](../com.sandjelkovic.kxjtime/kotlin.-long/index.md) (extensions in package com.sandjelkovic.kxjtime) |  |
