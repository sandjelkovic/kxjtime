[com.sandjelkovic.kxjtime](./index.md)

## Package com.sandjelkovic.kxjtime

### Extensions for External Classes

| Name | Summary |
|---|---|
| [java.time.Duration](java.time.-duration/index.md) |  |
| [kotlin.Int](kotlin.-int/index.md) |  |
| [kotlin.Long](kotlin.-long/index.md) |  |
