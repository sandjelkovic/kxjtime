[com.sandjelkovic.kxjtime](../index.md) / [java.time.Duration](index.md) / [after](./after.md)

# after

`infix fun Duration.after(zonedDateTime: ZonedDateTime): ZonedDateTime` [(source)](https://github.com/sandjelkovic/kxjtime/tree/master/src/main/kotlin/com/sandjelkovic/kxjtime/DurationExtensions.kt#L30)

Add the Duration to ZonedDateTime

**Return**
ZonedDateTime plus the Duration

`infix fun Duration.after(instant: Instant): Instant` [(source)](https://github.com/sandjelkovic/kxjtime/tree/master/src/main/kotlin/com/sandjelkovic/kxjtime/DurationExtensions.kt#L42)

Add the Duration to Instant

**Return**
Instant plus the Duration

`infix fun Duration.after(time: LocalDateTime): LocalDateTime` [(source)](https://github.com/sandjelkovic/kxjtime/tree/master/src/main/kotlin/com/sandjelkovic/kxjtime/DurationExtensions.kt#L54)

Add the Duration to LocalDateTime

**Return**
LocalDateTime plus the Duration

