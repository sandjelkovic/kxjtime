[com.sandjelkovic.kxjtime](../index.md) / [java.time.Duration](index.md) / [and](./and.md)

# and

`infix fun Duration.and(other: Duration): Duration` [(source)](https://github.com/sandjelkovic/kxjtime/tree/master/src/main/kotlin/com/sandjelkovic/kxjtime/DurationExtensions.kt#L60)

Add two durations

**Return**
Duration added to other Duration

