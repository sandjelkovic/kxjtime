[com.sandjelkovic.kxjtime](../index.md) / [java.time.Duration](index.md) / [before](./before.md)

# before

`infix fun Duration.before(zonedDateTime: ZonedDateTime): ZonedDateTime` [(source)](https://github.com/sandjelkovic/kxjtime/tree/master/src/main/kotlin/com/sandjelkovic/kxjtime/DurationExtensions.kt#L24)

Subtract the Duration from ZonedDateTime

**Return**
ZonedDateTime minus the Duration

`infix fun Duration.before(instant: Instant): Instant` [(source)](https://github.com/sandjelkovic/kxjtime/tree/master/src/main/kotlin/com/sandjelkovic/kxjtime/DurationExtensions.kt#L36)

Subtract the Duration from Instant

**Return**
Instant minus the Duration

`infix fun Duration.before(time: LocalDateTime): LocalDateTime` [(source)](https://github.com/sandjelkovic/kxjtime/tree/master/src/main/kotlin/com/sandjelkovic/kxjtime/DurationExtensions.kt#L48)

Subtract the Duration from LocalDateTime

**Return**
LocalDateTime minus the Duration

