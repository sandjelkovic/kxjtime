[com.sandjelkovic.kxjtime](../index.md) / [java.time.Duration](index.md) / [unaryMinus](./unary-minus.md)

# unaryMinus

`operator fun Duration.unaryMinus(): Duration` [(source)](https://github.com/sandjelkovic/kxjtime/tree/master/src/main/kotlin/com/sandjelkovic/kxjtime/DurationExtensions.kt#L18)

Returns a copy of this duration with the length negated.

**Return**
Negated {@code Duration} based on this duration

