[com.sandjelkovic.kxjtime](../index.md) / [kotlin.Int](index.md) / [hours](./hours.md)

# hours

`inline val `[`Int`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-int/index.html)`.hours: Duration` [(source)](https://github.com/sandjelkovic/kxjtime/tree/master/src/main/kotlin/com/sandjelkovic/kxjtime/DurationFactoryExtensions.kt#L18)