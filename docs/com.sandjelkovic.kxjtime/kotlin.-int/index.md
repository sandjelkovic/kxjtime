[com.sandjelkovic.kxjtime](../index.md) / [kotlin.Int](./index.md)

### Extensions for kotlin.Int

| Name | Summary |
|---|---|
| [days](days.md) | `val `[`Int`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-int/index.html)`.days: Duration` |
| [hours](hours.md) | `val `[`Int`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-int/index.html)`.hours: Duration` |
| [milliseconds](milliseconds.md) | `val `[`Int`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-int/index.html)`.milliseconds: Duration` |
| [minutes](minutes.md) | `val `[`Int`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-int/index.html)`.minutes: Duration` |
| [months](months.md) | `val `[`Int`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-int/index.html)`.months: Period` |
| [nanoseconds](nanoseconds.md) | `val `[`Int`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-int/index.html)`.nanoseconds: Duration` |
| [seconds](seconds.md) | `val `[`Int`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-int/index.html)`.seconds: Duration` |
| [weeks](weeks.md) | `val `[`Int`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-int/index.html)`.weeks: Period` |
| [years](years.md) | `val `[`Int`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-int/index.html)`.years: Period` |
