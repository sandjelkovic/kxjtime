[com.sandjelkovic.kxjtime](../index.md) / [kotlin.Int](index.md) / [minutes](./minutes.md)

# minutes

`inline val `[`Int`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-int/index.html)`.minutes: Duration` [(source)](https://github.com/sandjelkovic/kxjtime/tree/master/src/main/kotlin/com/sandjelkovic/kxjtime/DurationFactoryExtensions.kt#L15)