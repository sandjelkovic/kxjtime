[com.sandjelkovic.kxjtime](../index.md) / [kotlin.Int](index.md) / [months](./months.md)

# months

`inline val `[`Int`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-int/index.html)`.months: Period` [(source)](https://github.com/sandjelkovic/kxjtime/tree/master/src/main/kotlin/com/sandjelkovic/kxjtime/PeriodFactoryExtensions.kt#L9)