[com.sandjelkovic.kxjtime](../index.md) / [kotlin.Int](index.md) / [weeks](./weeks.md)

# weeks

`inline val `[`Int`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-int/index.html)`.weeks: Period` [(source)](https://github.com/sandjelkovic/kxjtime/tree/master/src/main/kotlin/com/sandjelkovic/kxjtime/PeriodFactoryExtensions.kt#L6)