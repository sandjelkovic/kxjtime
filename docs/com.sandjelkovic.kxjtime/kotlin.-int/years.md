[com.sandjelkovic.kxjtime](../index.md) / [kotlin.Int](index.md) / [years](./years.md)

# years

`inline val `[`Int`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-int/index.html)`.years: Period` [(source)](https://github.com/sandjelkovic/kxjtime/tree/master/src/main/kotlin/com/sandjelkovic/kxjtime/PeriodFactoryExtensions.kt#L12)