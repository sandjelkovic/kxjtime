[com.sandjelkovic.kxjtime](../index.md) / [kotlin.Long](./index.md)

### Extensions for kotlin.Long

| Name | Summary |
|---|---|
| [days](days.md) | `val `[`Long`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-long/index.html)`.days: Duration` |
| [hours](hours.md) | `val `[`Long`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-long/index.html)`.hours: Duration` |
| [milliseconds](milliseconds.md) | `val `[`Long`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-long/index.html)`.milliseconds: Duration` |
| [minutes](minutes.md) | `val `[`Long`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-long/index.html)`.minutes: Duration` |
| [nanoseconds](nanoseconds.md) | `val `[`Long`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-long/index.html)`.nanoseconds: Duration` |
| [seconds](seconds.md) | `val `[`Long`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-long/index.html)`.seconds: Duration` |
