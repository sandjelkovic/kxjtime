[com.sandjelkovic.kxjtime](../index.md) / [kotlin.Long](index.md) / [nanoseconds](./nanoseconds.md)

# nanoseconds

`inline val `[`Long`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-long/index.html)`.nanoseconds: Duration` [(source)](https://github.com/sandjelkovic/kxjtime/tree/master/src/main/kotlin/com/sandjelkovic/kxjtime/DurationFactoryExtensions.kt#L24)